
The excluded_users module is a helper module for other modules.  It
allows the administrators of a site to choose users not to be displayed
in user listings by modules that know to make use of excluded_users.
This is useful if you have test users, or something similar.

Modules which want to make use of this should use the following code:

(for Drupal 4.7.x)

$uids_to_exclude = array(0);
if (module_exist('excluded_users')) {
  $uids_to_exclude = excluded_users_get_excluded_uids();
}

...

$users_query = db_query("SELECT uid FROM {users} WHERE uid not in (" . implode(",", $uids_to_exclude) . ")");

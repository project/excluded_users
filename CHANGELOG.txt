2006 Nov 1
    Initial commit

2007 March 28
    Commited patch from http://drupal.org/node/93351

2007 March 29
    OK, this should now work for version 5
    had to change calls to drupal_get_form()

2007 May 18
    updated to add site_user_list hook

2008 Dec 09
    updated to Drupal 6
